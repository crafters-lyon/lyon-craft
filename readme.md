# Lyon Craft

Website for the Lyon Craft conference

* [WebSite](https://lyon-craft.fr)

## Install

```sh
npm install
```

## Build

```sh
npm run build
```

## Serve

```sh
npm run serve
```
